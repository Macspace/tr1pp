# Tr1pp Test

Tr1pp is a simple logging API for supermarket purchase receipts

## Features
1. Ability to log purchase receipts
2. Ability to query DB for total sales in a given month
3. Ability to query DB for complete product list
4. Ability to add/modify/delete products
5. Ability to query DB for monthly sales by product

## Installation

Clone repo to your local machine:

```git
git clone https://bitbucket.org/macspace/tr1pp.git
```

**Install dependencies and run locally**<br/>
*Note>> Install npm if not already installed on local machine*

Then run:

```npm
npm install
```

Create .env like the .env.sample file, just replace with your own enviroment variables.

Now start the server:

```npm
npm run start     /* Keep watching files for changes */
```

## Testing

To run tests:

```npm
npm run test       
```
![TEST RESULT](https://bitbucket.org/Macspace/tr1pp/src/master/test-result.png)
## API

API is deployed at [here](https://tr1pp-test.herokuapp.com/) on heroku.
API documentation (POSTMAN)  is here [here](https://documenter.getpostman.com/view/4271685/SVSNK7mi)

### API Routes

<table>
	<tr>
		<th>HTTP VERB</th>
		<th>ENDPOINT</th>
		<th>FUNCTIONALITY</th>
	</tr>
	<tr>
		<td>POST</td>
		<td>/api/v1/products</td> 
		<td>Create a product</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/api/v1/products</td> 
		<td>Get all products</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/api/v1/products/:productId</td> 
		<td>Update a Product</td>
	</tr>
    <tr>
		<td>DELETE</td>
		<td>/api/v1/products/:productId</td> 
		<td>Delete a Product</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/api/v1/receipts</td> 
		<td>To view all receipts</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/api/v1/products/buy/:productId</td> 
		<td>Create a receipt</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/api/v1/monthly-sales</td> 
		<td>Get monthly sales of all products</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/api/v1/monthly-sales/:productId</td> 
		<td>Get monthly sales of a particular product</td>
	</tr>
</table>  
