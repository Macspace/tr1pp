const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const receiptSchema = new Schema({
    buyer_name: {
        type: String,
        required: true,    
    },
    buyer_email: {
        type: String,    
    },
    buyer_phone: {
        type: String,
        required: true,
        maxlength: 15,   
    },
    buyer_address: {
        type: String,
        required: true,    
    },
    product_name: {
        type: String,
        required: true,    
    },
    product_id: {
        type: String,
        required: true,    
    },
    product_price: {
        type: Number,
        required: true,
    },
    purchased_at: {
        type: Date,
    },
    receipt_id: {
        type: String,
    }
})

receiptSchema.pre("save", async function (next){
    this.purchased_at = await new Date().toISOString();
    this.receipt_id = await receiptIdGenerator(this.product_id);
    next();
})

function receiptIdGenerator(product_id)  {
   const date = new Date(); 
   return date.toISOString() + product_id;
}
module.exports = mongoose.model('Receipt', receiptSchema);