const express = require('express');
const router = express.Router();
const ProductController = require('./controllers/ProductController');
const ReceiptController = require('./controllers/ReceiptController');
const SalesController = require('./controllers/SalesController');


         // Product creation endpoints
router.post('/products', ProductController.createAProduct)
  .get('/products', ProductController.getAllProduct)
  .put('/products/:productId', ProductController.updateProduct)
  .delete('/products/:productId', ProductController.deleteAProduct)
  // Receipt creation endpoints
  .post('/products/buy/:productId', ReceiptController.createAReceipt)
  .get('/receipts', ReceiptController.getAllReceipt)
  //Calculate All monthly sales endpoint
  .get('/monthly-sales', SalesController.calculateMonthlySales)
  //Calculate monthly sales for a product endpoint
  .get('/monthly-sales/:productId', SalesController.calculateMonthlyProductSales);



module.exports = router;