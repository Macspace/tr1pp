// const Product = require('../models/Products');
const Joi = require('@hapi/joi')
const Receipt = require('../models/Receipts')
const Product = require('../models/Products')

class ReceiptController {

  async createAReceipt(req, res) {
    const data = req.body;
    const schema = Joi.object().keys({
        buyer_name: Joi.string().required().max(50),
        buyer_phone: Joi.string().required().max(15),
        buyer_email: Joi.string().email(),
        buyer_address: Joi.string().required().max(100),
        product_id: Joi.string().required()
    })
    const {error, value} = Joi.validate({product_id: req.params.productId, ...data}, schema);
    if (error) return res.status(422).send({ status: 'fail', error: error.message });
    
    try {
      await Product.findById(req.params.productId, (e, response)=>{
        if (e) return res.status(404).send({ status: 'fail', error: 'Product not found!' });
        console.log(response);
        value.product_name = response.name;
        value.product_price = response.price;
      });
      const receipt = await new Receipt(value);
      receipt.save((err, resp)=> {
      if (err) return res.status(500).send({ status: 'fail', error: error });
      return res.status(201).send({ status: 'success',
        message: 'Product bought successfully, below is your receipt',
        data: resp
      }); 
    })
    } catch (err) {
        return res.status(500).send({ status: 'fail', error: err });
    }
  }

  async getAllReceipt(req, res) {
    try {
      const allReceipt = await Receipt.find();
      return res.status(200).send({ status: 'success', data: allReceipt });
    } catch (error) {
        return res.status(500).send({ status: 'fail', error: error });
    }
  }
}

module.exports = new ReceiptController();