// const Product = require('../models/Products');
const Joi = require('@hapi/joi')
const Product = require('../models/Products')

class ProductController {

  async createAProduct(req, res) {
    const data = req.body;
    const schema = Joi.object().keys({
        name: Joi.string().required().max(60),
        description: Joi.string(),
        price: Joi.number().required()
    })
    const {error, value} = Joi.validate(data, schema);
    if (error) return res.status(422).send({ status: 'fail', error: error.message });
    let product = new Product(value);
    product.save((err, resp)=> {
        if (err) return res.status(500).send({ status: 'fail', error: err });
        return res.status(201).send({ status: 'success',
         message: 'Product created successfully',
         data: resp
        }); 
    })

  }

  async getAllProduct(req, res) {
    try {
      const allProduct = await Product.find();
      return res.status(200).send({ status: 'success', data: allProduct });
    } catch (error) {
        return res.status(500).send({ status: 'fail', error: error });
    }
  }

  async updateProduct(req, res) {
    const data = req.body;
    const schema = Joi.object().keys({
        productId: Joi.string().required(),
        name: Joi.string().max(60),
        description: Joi.string(),
        price: Joi.number()
    })
    const {error, value} = Joi.validate({ productId: req.params.productId, ...data}, schema);
    if (error) return res.status(422).send({ status: 'fail', error: error.message });

    try {
      const product =  await Product.findByIdAndUpdate(value.productId, {$set: req.body }, (err, resp)=>{
          if (err) return res.status(500).send({ status: 'fail', error: err });
          return res.status(202).send({ status: 'success', message: 'Your product has been updated successfully' });
      });
    } catch (error) {
        return res.status(500).send({ status: 'fail', error: error });
    }
  }

  async deleteAProduct(req, res) {
    const data = { productId: req.params.productId};
    const schema = Joi.object().keys({
        productId: Joi.string().required()
    })
    const {error, value} = Joi.validate(data, schema);
    if (error) return res.status(422).send({ status: 'fail', error: error.message });
    try {
      const product = await Product.findByIdAndRemove(value.productId);
      return res.status(200).send({ status: 'success', message: 'Product removed successfully'}); 
    } catch (error) {
        return res.status(500).send({ status: 'fail', error: error });
    }
  }

}

module.exports = new ProductController();