// const Product = require('../models/Products');
const Joi = require('@hapi/joi')
const Receipt = require('../models/Receipts')

class SalesController {

  async calculateMonthlySales(req, res) {
    try {
      const date = new Date();
      if (date.getMonth() >= 1) {
        await date.setMonth(date.getMonth()-1)
      } else {
        await date.setFullYear(date.getFullYear()-1)
        await date.setMonth(12)
      }
      const allSales = await Receipt.find({ purchased_at: { "$gte": date } })
      if(!allSales) res.status(200).send({ status: 'success', message: "There is no sales all through last month" });
      const arrSum = await allSales.map(el => el.product_price).reduce((a,b) => a + b, 0)
      return res.status(200).send({ status: 'success', data: 
        { product_sold: allSales.length, total_sales: arrSum }
      });
    } catch (error) {
      return res.status(500).send({ status: 'fail', error: error });
    }
  }

  async calculateMonthlyProductSales(req, res) {
    const id = { productId: req.params.productId};
    const schema = Joi.object().keys({
        productId: Joi.string().required()
    })
    const {error, value} = Joi.validate(id, schema);
    if (error) return res.status(422).send({ status: 'fail', error: error.message });
    try {
      const date = new Date();
      if (date.getMonth() >= 1) {
        await date.setMonth(date.getMonth()-1)
      } else {
        await date.setFullYear(date.getFullYear()-1)
        await date.setMonth(12)
      }
      const allProductSales = await Receipt.find({ product_id : value.productId,  purchased_at: { "$gte": date }, })
      if(!allProductSales) res.status(200).send({ status: 'success', message: "There is no sales all through last month" });
      const arrSum = await allProductSales.map(el => el.product_price).reduce((a,b) => a + b, 0)
      return res.status(200).send({ status: 'success', data: 
        { quantity_sold: allProductSales.length, total_sales: arrSum }
      });
    } catch (error) {
      return res.status(500).send({ status: 'fail', error: error });
    }
  }
}

module.exports = new SalesController();