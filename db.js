const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config()

const { DATABASE_URL } = process.env;

 const connectToDB = ()=> {
     mongoose.connect(DATABASE_URL, { useNewUrlParser: true })
      .then(res=> console.log('Connected successfully'))
      .catch((err)=> {
        console.log('Oopps, DOG ate your DB, Exiting app now!');
        process.emit(-1);
      })
 }; 

module.exports = connectToDB;