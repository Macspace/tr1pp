const express = require('express');
const logger = require('morgan');
const dotenv = require('dotenv');
const routes = require('./src/routes');
const connectToDB = require('./db');
dotenv.config()

const app = express();
const PORT = process.env.PORT || 8200;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger('combined'))
connectToDB()

app.get('/', (req, res) => {
 res.status(200).send({ status: 'success', message: 'Welcome home, pal'})
})

app.use('/api/v1', routes);

app.listen(PORT, ()=>{
    console.log(`Super Tr1pp app running hot on ${PORT}`);
})

module.exports = app;