const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');
// Configure chai
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;
let id;

describe("Sales", () => {

    beforeEach(async () => {
        const { body } = await chai.request(app)
           .post('/api/v1/products').set('Content-Type', 'application/json')
           .send({ name: 'Golden Spoon', description: 'The spoon to stir your mocha latte', price: 500 });
        id = await body.data._id;
        });

    describe("GET /monthly-sales", () => {
        // Test to get all monthly
        it("should create a receipt", async() => {
          const res = await chai.request(app)
           .get('/api/v1/monthly-sales');
        expect(res.status).to.equal(200);
        expect(res.body).should.have.property('status');
        expect(res.body.data).should.be.an('object');
        expect(res.body.status).to.equal('success');
        });
    });
      
    describe("GET /monthly-sales/:productId", () => {
        // Test to DET monthly sales of a particular product
        it("should not  create a receipt", async() => {
            console.log(id)
          const res = await chai.request(app)
           .get('/api/v1/monthly-sales/'+id)
        expect(res.status).to.equal(200);
        expect(res.body).should.have.property('status');
        expect(res.body.status).to.equal('success');
         });
    });

});