const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');
// Configure chai
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;
let id;

describe("Products", () => {

    describe("POST /products", () => {
        // Test to create a product
        it("should create a product", async() => {
          const res = await chai.request(app)
           .post('/api/v1/products')
           .set('Content-Type', 'application/json')
           .send({ name: 'Golden Spoon', description: 'The spoon to stir your mocha latte', price: 500 });
        id = res.body.data._id;
        expect(res.status).to.equal(201);
        expect(res.body).should.have.property('status');
        expect(res.body.data).should.be.an('object');
        expect(res.body.status).to.equal('success');
        expect(res.body.data.name).to.equal('Golden Spoon');
        expect(res.body.data.price).to.equal(500);
        expect(res.body.data.description).to.equal('The spoon to stir your mocha latte');       
         });
      
    });
    describe("POST /products", () => {
        // Test to not create a product because price is absent
        it("should not create a product", async() => {
          const res = await chai.request(app)
            .post('/api/v1/products')
            .set('Content-Type', 'application/json')
            .send({ name: 'Golden Spoon', description: 'The spoon to stir your mocha latte' });
        expect(res.status).to.equal(422);
        expect(res.body).should.have.property('status');
        expect(res.body.status).to.equal('fail'); 
         });
    });

    describe("POST /products", () => {
        // Test to not create a product because name is absent
        it("should not create a product", async() => {
          const res = await chai.request(app)
            .post('/api/v1/products')
            .set('Content-Type', 'application/json')
            .send({ description: 'The spoon to stir your mocha latte', price: 500 });
          expect(res.status).to.equal(422);
          expect(res.body).should.have.property('status');
          expect(res.body.status).to.equal('fail'); 
         });
    });

    describe("GET /products", () => {
        // Test to get all product
        it("should get all product", async() => {
          const res = await chai.request(app)
            .get('/api/v1/products')            
          expect(res.status).to.equal(200);
          expect(res.body).should.have.property('status');
          expect(res.body.status).to.equal('success');
          expect(res.body.data[0].name).to.equal('Golden Spoon');
          expect(res.body.data[0].price).to.equal(500);
          expect(res.body.data[0].description).to.equal('The spoon to stir your mocha latte'); 
         });
    });

    describe("PUT /products", () => {
        // Test to update a product
        it("should update a product", async() => {
          const res = await chai.request(app)
            .put('/api/v1/products/'+id)
            .set('Content-Type', 'application/json')
            .send({ description: 'The spoon to serve your golden visitor', price: '700' });
          expect(res.status).to.equal(202);
          expect(res.body).should.have.property('status');          
          expect(res.body.data).should.be.an('object');
          expect(res.body.status).to.equal('success');
          expect(res.body.message).to.equal('Your product has been updated successfully'); 
         });
    });
    describe("DELETE /products", () => {
        // Test to update a product
        it("should delete a product", async() => {
          const res = await chai.request(app)
            .delete('/api/v1/products/'+id)
            .set('Content-Type', 'application/json')
          expect(res.status).to.equal(200);
          expect(res.body).should.have.property('status');          
          expect(res.body.data).should.be.an('object');
          expect(res.body.status).to.equal('success');
          expect(res.body.message).to.equal('Product removed successfully');
         });
    });

});