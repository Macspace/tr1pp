const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');
// Configure chai
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;
let id;

describe("Receipts", () => {

    beforeEach(async () => {
        const { body } = await chai.request(app)
           .post('/api/v1/products')
           .set('Content-Type', 'application/json')
           .send({ name: 'Golden Spoon', description: 'The spoon to stir your mocha latte', price: 500 });
        id = await body.data._id;
        });

    describe("POST /receipts", () => {
        // Test to create a receipt
        it("should create a receipt", async() => {
          const res = await chai.request(app)
           .post('/api/v1/products/buy/'+id)
           .set('Content-Type', 'application/json')
           .send({ buyer_name: 'David Goldberg', buyer_phone: '+2348160532895', buyer_address: 'Overhost, Mars' });
        expect(res.status).to.equal(201);
        expect(res.body).should.have.property('status');
        expect(res.body.data).should.be.an('object');
        expect(res.body.status).to.equal('success');
        expect(res.body.data.buyer_name).to.equal('David Goldberg');
        expect(res.body.data.product_price).to.equal(500);
        expect(res.body.data.product_name).to.equal('Golden Spoon');
        expect(res.body.data.buyer_phone).to.equal('+2348160532895');

         });
    });
      
    describe("POST /receipts", () => {
        // Test to  NOT create a receipt because buyer phone is absent
        it("should not  create a receipt", async() => {
          const res = await chai.request(app)
           .post('/api/v1/products/buy/'+id)
           .set('Content-Type', 'application/json')
           .send({ buyer_name: 'David Goldberg', buyer_address: 'Overhost, Mars' });
        expect(res.status).to.equal(422);
        expect(res.status).to.equal(422);
        expect(res.body).should.have.property('status');
        expect(res.body.status).to.equal('fail'); 

         });
    });
      
    describe("GET /receipts", () => {
        // Test to create a receipt
        it("should get all receipt", async() => {
          const res = await chai.request(app)
           .get('/api/v1/receipts');
        expect(res.status).to.equal(200);
        expect(res.body).should.have.property('status');
        expect(res.body.status).to.equal('success');
        expect(res.body.data[0].buyer_name).to.equal('David Goldberg');
        expect(res.body.data[0].product_price).to.equal(500);
        expect(res.body.data[0].buyer_phone).to.equal('+2348160532895');
        });
      
    });

});